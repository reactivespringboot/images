package com.szagoret.springboot2;

import com.szagoret.springboot2.images.Image;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public abstract class AbstractTestImageRepository {
    Logger logger = LoggerFactory.getLogger(AbstractTestImageRepository.class);

    void setUpDataBase(MongoOperations operations) {
        operations.dropCollection(Image.class);
        operations.insert(new Image("1", "image1.jpg"));
        operations.insert(new Image("2", "image2.jpg"));
        operations.insert(new Image("3", "image3.jpg"));

        operations.findAll(Image.class).forEach(image -> logger.info(image.toString()));
    }

}
