package com.szagoret.springboot2;

import com.szagoret.springboot2.images.Comment;
import com.szagoret.springboot2.images.Image;
import com.szagoret.springboot2.images.ImageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@WebFluxTest(controllers = HomeController.class)
@Import(ThymeleafAutoConfiguration.class)
public class HomeControllerTests {

    @Autowired
    WebTestClient webTestClient;

    @MockBean
    ImageService imageService;


    @MockBean
    MongoOperations mongoOperations;


    @Test
    public void baseRouteShouldListAllImages() {
        // given
        Image alphaImage = new Image("1", "alpha.png");
        Image bravoImage = new Image("2", "bravo.png");
        given(imageService.findAllImages())
                .willReturn(Flux.just(alphaImage, bravoImage));

        Comment alphaComment = new Comment();
        alphaComment.setId("1");
        alphaComment.setImageId("1");
        alphaComment.setComment("A very good alpha comment!");

        Comment bravoComment = new Comment();
        bravoComment.setId("2");
        bravoComment.setImageId("2");
        bravoComment.setComment("A very good bravo comment!");

        // when
        EntityExchangeResult<String> result = webTestClient
                .get().uri("/")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class).returnResult();

        // then
        verify(imageService).findAllImages();
        verifyNoMoreInteractions(imageService);
        assertThat(result.getResponseBody())
                .contains(
                        "<title>Reactive:SpringBoot</title>")
                .contains("<a href=\"images/alpha.png/raw\">")
                .contains("<a href=\"images/bravo.png/raw\">");
    }

    @Test
    public void fetchingImageShouldWork() {
        given(imageService.findOneImage(any()))
                .willReturn(Mono.just(
                        new ByteArrayResource("data".getBytes())));

        webTestClient
                .get().uri("images/alpha.png/raw")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class).isEqualTo("data");
        verify(imageService).findOneImage("alpha.png");
        verifyNoMoreInteractions(imageService);
    }


    @Test
    public void fetchingNullImageShouldFail() throws IOException {
        Resource resource = mock(Resource.class);
        given(resource.getInputStream())
                .willThrow(new IOException("Bad file"));
        given(imageService.findOneImage(any()))
                .willReturn(Mono.just(resource));

        webTestClient
                .get().uri("/images/alpha.png/raw")
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(String.class)
                .isEqualTo("Couldn't find alpha.png => Bad file");

        verify(imageService).findOneImage("alpha.png");
        verifyNoMoreInteractions(imageService);
    }

    @Test
    public void deleteImageShouldWork() {
        Image alphaImage = new Image("1", "image1.jpg");
        given(imageService.deleteImage(alphaImage.getName())).willReturn(Mono.empty());

        webTestClient
                .delete().uri("/images/image1.jpg")
                .exchange()
                .expectStatus().isSeeOther()
                .expectHeader().valueEquals(HttpHeaders.LOCATION, "/");

        verify(imageService).deleteImage(alphaImage.getName());
        verifyNoMoreInteractions(imageService);
    }
}
