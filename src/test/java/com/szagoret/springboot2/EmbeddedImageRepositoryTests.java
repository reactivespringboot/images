package com.szagoret.springboot2;

import com.szagoret.springboot2.images.Image;
import com.szagoret.springboot2.images.ImageRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoOperations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * DataMongoTest will disable the general Spring Boot autoconfiguration,
 * and instead, use Spring Boot's test-based autoconfigurations to create a MongoTemplate,
 * a MongoDB connection, MongoDB property settings, a ReactiveMongoTemplate and an embedded MongoDB instance;
 * it will also enable the MongoDB repositories
 */

@DataMongoTest
public class EmbeddedImageRepositoryTests extends AbstractTestImageRepository {

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    MongoOperations operations;

    @Before
    public void setUp() {
        super.setUpDataBase(operations);
    }

    @Test
    public void findAllShouldWork() {
        Flux<Image> images = imageRepository.findAll();

        /**
         * We use Reactor Test's @StepVerifier to subscribe to the Flux from the repository and then assert against it.
         *
         * recordWith method fetches the entire Flux and converts it into an ArrayList via a method handle.
         */

        StepVerifier.create(images)
                .recordWith(ArrayList::new)
                .expectNextCount(3)
                .consumeRecordedWith(results -> {
                    assertThat(results).hasSize(3);
                    assertThat(results)
                            .extracting(Image::getName)
                            .contains(
                                    "image1.jpg",
                                    "image2.jpg",
                                    "image3.jpg"
                            );
                })
                .expectComplete()
                .verify();
    }


    @Test
    public void findByNameShouldWork() {
        Mono<Image> image = imageRepository.findByName("image3.jpg");

        StepVerifier.create(image)
                .expectNextMatches(result -> {
                    assertThat(result.getName()).isEqualTo("image3.jpg");
                    assertThat(result.getId()).isEqualTo("3");

                    return true;
                });
    }

}
