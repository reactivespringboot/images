package com.szagoret.springboot2;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.reactive.HiddenHttpMethodFilter;


@SpringCloudApplication
public class ReactiveSpringImagesService {

    public static void main(String[] args) {

        SpringApplication.run(ReactiveSpringImagesService.class, args);
    }


    /**
     * make the HTTP DELETE methods work properly
     */
    @Bean
    HiddenHttpMethodFilter hiddenHttpMethodFilter() {
        return new HiddenHttpMethodFilter();
    }
}
