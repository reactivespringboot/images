package com.szagoret.springboot2;

import com.szagoret.springboot2.images.CommentHelper;
import com.szagoret.springboot2.images.ImageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.WebSession;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Controller
public class HomeController {

    private final ImageService imageService;

    // tag::injection[]
    private final CommentHelper commentHelper;

    public HomeController(ImageService imageService,
                          CommentHelper commentHelper) {
        this.imageService = imageService;
        this.commentHelper = commentHelper;
    }
    // end::injection[]

    @GetMapping("/")
    public Mono<String> index(Model model, WebSession webSession) {
        model.addAttribute("images",
                imageService
                        .findAllImages()
                        .map(image -> new HashMap<String, Object>() {{
                            put("id", image.getId());
                            put("name", image.getName());
                            put("owner", image.getOwner());
                            // tag::comments[]
                            put("comments", commentHelper.getComments(image));
                            // end::comments[]
                        }})
        );
        return Mono.just("index");
    }

    @GetMapping("/token")
    @ResponseBody
    public Mono<Map<String, String>> token(WebSession webSession) {
        return Mono.just(Collections.singletonMap("token", webSession.getId()));
    }
}
