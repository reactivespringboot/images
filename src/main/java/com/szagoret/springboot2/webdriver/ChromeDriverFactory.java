package com.szagoret.springboot2.webdriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;

class ChromeDriverFactory implements ObjectFactory<ChromeDriver> {

    private ChromeDriverService chromeDriverService;
    private WebDriverConfigurationProperties properties;

    public ChromeDriverFactory(ChromeDriverService chromeDriverService,
                               WebDriverConfigurationProperties properties) {
        this.chromeDriverService = chromeDriverService;
        this.properties = properties;
    }

    @Override
    public ChromeDriver getObject() throws BeansException {
        if (properties.getChrome().isEnabled()) {
            try {
                return new ChromeDriver(chromeDriverService);
            } catch (IllegalStateException e) {
                e.printStackTrace();
                // swallow the exception
            }
        }
        return null;
    }
}
