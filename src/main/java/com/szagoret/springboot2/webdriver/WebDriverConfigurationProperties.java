package com.szagoret.springboot2.webdriver;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("com.szagoret.webdriver")
public class WebDriverConfigurationProperties {

    private Firefox firefox = new Firefox();
    private Safari safari = new Safari();
    private Chrome chrome = new Chrome();

    @Data
    static class Firefox {
        private boolean enabled = false;
    }

    @Data
    static class Safari {
        private boolean enabled = false;
    }

    /**
     * set this flag to true if you want to run the service on application starting
     */
    @Data
    static class Chrome {
        private boolean enabled = false;
    }
}
