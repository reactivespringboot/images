package com.szagoret.springboot2.images;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@AllArgsConstructor
public class Image {
    @Id
    private String id;
    private String name;
    private String owner;
}
