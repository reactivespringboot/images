package com.szagoret.springboot2.images;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Principal;
import java.time.Duration;
import java.util.List;
import java.util.UUID;

import static com.szagoret.springboot2.LocalPaths.UPLOAD_DIR;

@Service
public class ImageService {

    private final ResourceLoader resourceLoader;
    private final MeterRegistry meterRegistry;

    @Autowired
    private ImageRepository imageRepository;


    public ImageService(ResourceLoader resourceLoader, MeterRegistry meterRegistry) {
        this.resourceLoader = resourceLoader;
        this.meterRegistry = meterRegistry;
    }


    /**
     * Reactor's block() API is what we do when we want to transform a Mono<T> into just T.
     * It's a simple one-to-one concept. Inside the method, it invokes the reactive streams' subscribe() API,
     * meaning it will cause any chain of operations to take effect.
     *
     * @return List<Image>
     */
    public List<Image> findAllImagesSync() {
        return findAllImages().collectList().block(Duration.ofSeconds(10));
    }

    public Flux<Image> findAllImages() {
        return imageRepository.findAll().log("findAll");
//        try {
//            return Flux.fromIterable(
//                    Files.newDirectoryStream(Paths.get(UPLOAD_DIR)))
//                    .map(path ->
//                            new Image(String.valueOf(path.hashCode()),
//                                    path.getFileName().toString(), "Unknown"));
//        } catch (IOException e) {
//            return Flux.empty();
//        }
    }

    /**
     * If we wrote Mono.just(resourceLoader.getResource(...?)),
     * the resource fetching would happen immediately when the method is called.
     * By putting it inside a Java 8 Supplier, that won't happen until the lambda is invoked.
     * And because it's wrapped by a Mono, invocation won't happen until the client
     * subscribes.
     *
     * @param imageName
     * @return
     */
    public Mono<Resource> findOneImage(String imageName) {
        return Mono.fromSupplier(() ->
                resourceLoader.getResource("file:" + UPLOAD_DIR + "/" + imageName)
        );
    }

    Resource findOneImageSync(String imageName) {
        return findOneImage(imageName).block(Duration.ofSeconds(10));
    }


    public Mono<Void> createImage(Flux<FilePart> files, Principal principal) {
        return files
                .log("createImage-files")
                .flatMap(file -> {
                    Mono<Image> saveDatabaseImage = imageRepository.save(new Image(
                            UUID.randomUUID().toString(),
                            file.filename(),
                            principal.getName()))
                            .log("createImage-save");

                    Mono<Void> copyFile = Mono.just(
                            Paths.get(UPLOAD_DIR, file.filename()).toFile())
                            .log("createImage-picktarget")
                            .map((File destFile) -> {
                                try {
                                    destFile.createNewFile();
                                    return destFile;
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            })
                            .log("createImage-newfile")
                            .flatMap(file::transferTo)
                            .log("createImage-copy");

                    Mono<Void> countFile = Mono.fromRunnable(() -> {
                        meterRegistry
                                .summary("files.uploaded.bytes")
                                .record(Paths.get(UPLOAD_DIR,
                                        file.filename()).toFile().length());
                    });
                    return Mono.when(saveDatabaseImage, copyFile, countFile)
                            .log("createImage-when");
                }).log("createImage-flatMap")
                .then()
                .log("createImage-done");
    }

    /**
     * To hold off until subscribe, we need to wrap our code with Mono.fromRunnable()
     *
     * @param filename
     * @return
     */
    public Mono<Void> deleteImage(String filename) {
        return Mono.fromRunnable(() -> {
            try {
                Files.deleteIfExists(Paths.get(UPLOAD_DIR, filename));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

}
